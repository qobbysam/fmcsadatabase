package fmcsa

import (
	"fmt"
	"strconv"
	"sync"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

//var config gorm.Config = gorm.Config{}

var Abc string = "test here"

var dsn string = "host=localhost user=postgres password=moonlight dbname=go_red_1  sslmode=disable TimeZone=Asia/Shanghai "

var DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

type GenFmcsaInterface interface {
	GenFmcsa() Fmcsa_object
}

func StreamFmcsa(bulk []Additions) chan []interface{} {

	buffer_len := len(bulk) * 2

	out_list := make([]interface{}, 0)

	Out_message_chan := make(chan []interface{}, 2000)

	local_chan := make(chan interface{}, buffer_len)

	var wg sync.WaitGroup

	for _, value := range bulk {

		wg.Add(1)

		go func(out chan interface{}, line Additions) {

			//out_fmcsa := line.GenFmcsa()

			constrct_out, fmcsa_out := line.GenFmcsa()

			out <- constrct_out
			out <- fmcsa_out

			//out <- line.GenFmcsa()

			wg.Done()

		}(local_chan, value)

		//go func(out chan Fmcsa_object, line_in Additions)

		// out_fmcsa, out_construct := value.GenFmcsa()

		// Out_message_chan <- out_construct
		// Out_message_chan <- out_fmcsa

		//	wg.Done()

		//}//(Out_message_chan, value)

		wg.Add(1)
		go func(in chan interface{}, out_chan chan []interface{}, outL []interface{}) {

			for msg := range in {

				outL = append(outL, msg)

				if len(outL)%1000 == 0 {

					out_chan <- outL

					outL = nil
				}
			}

			out_chan <- outL

			wg.Done()

		}(local_chan, Out_message_chan, out_list)
	}

	defer close(local_chan)
	defer close(Out_message_chan)
	wg.Wait()
	return Out_message_chan
}

// func ProduceFmcsa(inlist []Additions) []GenFmcsaInterface {

// 	out := make([]GenFmcsaInterface, 0)

// 	for _, value := range inlist {

// 		out = append(out, value)

// 		//outfmcsa, outconstruct = append(out, value.GenFmcsa())
// 	}

// 	return out

// }

type CleanObjectInterface interface {
	CleanObject() []string
}
type ConstructInfo struct {
	//gorm.Model

	Dot_number int `gorm:"primaryKey"`

	First_time   bool
	Active       bool
	Check_count  int
	Update_count int

	updates string `sql:"type:JSONB NOT NULL DEFAULT '{}'::JSONB"`
}

const MyTimeFormat = "01JAN2001"

// type MyTime time.Time

// func NewMyTime(hour, min, sec int) MyTime {
// 	t := time.Date(0, time.January, 1, hour, min, sec, 0, time.UTC)
// 	return MyTime(t)
// }

// func (t *MyTime) Scan(value interface{}) error {
// 	switch v := value.(type) {
// 	case []byte:
// 		return t.UnmarshalText(string(v))
// 	case string:
// 		return t.UnmarshalText(v)
// 	case time.Time:
// 		*t = MyTime(v)
// 	case nil:
// 		*t = MyTime{}
// 	default:
// 		return fmt.Errorf("cannot sql.Scan() MyTime from: %#v", v)
// 	}
// 	return nil
// }

// // func ToMytime(date_in time.Time) MyTime{

// // 	out:= date_in

// // 	return MyTime(out)

// // }

// func (t MyTime) Value() (driver.Value, error) {
// 	return driver.Value(time.Time(t).Format(MyTimeFormat)), nil
// }

// func (t *MyTime) UnmarshalText(value string) error {
// 	dd, err := time.Parse(MyTimeFormat, value)
// 	if err != nil {
// 		return err
// 	}
// 	*t = MyTime(dd)
// 	return nil
// }

// func (MyTime) GormDataType() string {
// 	return "TIME"
// }

type Fmcsa_object struct {
	ConstructInfoDot_number int
	ConstructInfo           ConstructInfo `gorm:"foreignKey:ConstructInfoDot_number"`

	Dot_number  int `gorm:"primaryKey"`
	Mc_number   string
	Mc_add_date time.Time `gorm:"type:time"`

	Legal_name string

	Dba_name          string
	Carrier_operation string
	Dot_add_date      time.Time `gorm:"type:time"`

	Oic_state       string
	Nbr_power_units string
	Driver_total    string
	Hm_flag         string
	Pc_flag         string

	Phy_street  string
	Phy_city    string
	Phy_state   string
	Phy_zip     string
	Phy_country string

	Mailing_street  string
	Mailing_city    string
	Mailing_state   string
	Mailing_zip     string
	Mailing_country string

	Mcs150_date         string
	Mcs150_mileage_year string
	Mcs150_mileage      string

	Telephone     string
	Fax           string
	Email_address string
}

func (e *Fmcsa_object) GenFmcsa() {

}

func CreateNewFmcsa(inlist []string) (Fmcsa_object, ConstructInfo) {

	layout := "01JAN2001"

	date, err := time.Parse(layout, inlist[22])

	var f_construct ConstructInfo

	var f_Fmcsa Fmcsa_object

	num, err := strconv.Atoi(inlist[0])
	if err != nil {
		fmt.Println("none here")
	}

	f_construct.Active = true
	f_construct.First_time = true
	f_construct.Update_count = 0
	f_construct.Dot_number = num

	f_Fmcsa.ConstructInfoDot_number = num
	//f_Fmcsa.ConstructInfo = f_construct

	f_Fmcsa.Dot_number = num
	f_Fmcsa.Legal_name = inlist[1]
	f_Fmcsa.Dba_name = inlist[2]
	f_Fmcsa.Carrier_operation = inlist[3]
	f_Fmcsa.Hm_flag = inlist[4]
	f_Fmcsa.Pc_flag = inlist[5]
	f_Fmcsa.Phy_street = inlist[6]
	f_Fmcsa.Phy_city = inlist[7]
	f_Fmcsa.Phy_state = inlist[8]
	f_Fmcsa.Phy_zip = inlist[9]
	f_Fmcsa.Phy_country = inlist[10]
	f_Fmcsa.Mailing_street = inlist[11]
	f_Fmcsa.Mailing_city = inlist[12]
	f_Fmcsa.Mailing_state = inlist[13]
	f_Fmcsa.Mailing_zip = inlist[14]
	f_Fmcsa.Mailing_country = inlist[15]
	f_Fmcsa.Telephone = inlist[16]
	f_Fmcsa.Fax = inlist[17]
	f_Fmcsa.Email_address = inlist[18]
	f_Fmcsa.Mcs150_date = inlist[19]
	f_Fmcsa.Mcs150_mileage = inlist[20]
	f_Fmcsa.Mcs150_mileage_year = inlist[21]
	f_Fmcsa.Dot_add_date = date
	f_Fmcsa.Oic_state = inlist[23]
	f_Fmcsa.Nbr_power_units = inlist[24]
	f_Fmcsa.Driver_total = inlist[25]

	return f_Fmcsa, f_construct

}

func UpdateOrCreateFmcsa_Objects(list_in *[]interface{}) {

	//in_list := make([]GenFmcsaInterface, 0)
	//in_list = list_in

	//DB.AutoMigrate()

	construct_list := make([]ConstructInfo, 0)
	fmcsa_list := make([]Fmcsa_object, 0)

	for _, value := range *list_in {

		switch value.(type) {

		case Fmcsa_object:
			fmcsa_list = append(fmcsa_list, value.(Fmcsa_object))

		case ConstructInfo:
			construct_list = append(construct_list, value.(ConstructInfo))

		default:
			continue
		}

		// valuex := reflect.ValueOf(value)

		// if valuex.Type().String() == "ConstructInfo"{

		// 	construct_list = append(construct_list, valuex

		// }

	}

	//in_list := list_in

	DB.Create(&construct_list)
	DB.Create(&fmcsa_list)

	fmt.Println("Commit good")
}

// type Mc struct {
// 	Mc_id           string
// 	Mc_number       string
// 	Mc_type         string
// 	Mc_in           string
// 	Mc_company_name string
// 	Mc_city         string
// 	Mc_state        string
// 	Mc_grant_date   string
// 	Mc_apply_date   string
// }

// func (m Mc) CleanObject() []string {
// 	out := make([]string, 0)
// 	out = append(out, m.Mc_company_name, m.Mc_city, m.Mc_grant_date)

// 	return out
// }
// func (m Fmcsa_object) CleanObject() []string {
// 	out := []string{m.Legal_name, m.Phy_city}

// 	// out = append(out, m.Legal_name)
// 	// out = append(out, m.Phy_city)
// 	out = append(out, m.Phy_state)
// 	out = append(out, m.Mailing_city)
// 	out = append(out, m.Mailing_state)

// 	return out
// }

// func (m Fmcsa_object) CleanObject() []string {
// 	out := make([]string, 0)

// 	out = append(out, m.Legal_name)
// 	out = append(out, m.Phy_city)
// 	out = append(out, m.Phy_state)
// 	out = append(out, m.Mailing_city)
// 	out = append(out, m.Mailing_state)
// 	return out
// }
