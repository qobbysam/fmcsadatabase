package fmcsa

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"

	"github.com/cespare/xxhash"
)

//var fs afero.Fs
var BufferSize int = 512

type Fromcsv struct {
	Line_in []string
}

func CleanStrings(text string) string {

	s := strings.TrimSpace(text)
	return s
}

func CleanDate(text string) string {
	dt, ok := time.Parse("01-JAN-02", text)

	if ok != nil {
		return text
	}

	out := dt.Format("01JAN2001")

	return out

}
func (e *Fromcsv) CleanSelfLines() []string {

	Dot_number := CleanStrings(e.Line_in[0])
	//Mc_number  := CleanStrings(e.Line_in[])
	Legal_name := CleanStrings(e.Line_in[1])
	Hm_flag := CleanStrings(e.Line_in[4])
	Pc_flag := CleanStrings(e.Line_in[5])
	Dba_name := CleanStrings(e.Line_in[2])
	Carrier_operation := CleanStrings(e.Line_in[3])
	Dot_add_date := CleanDate(e.Line_in[22])
	//Mc_add_date       := e.Line_in[])
	Oic_state := CleanStrings(e.Line_in[23])
	Nbr_power_units := CleanStrings(e.Line_in[24])
	Driver_total := CleanStrings(e.Line_in[25])

	Phy_street := CleanStrings(e.Line_in[6])
	Phy_city := CleanStrings(e.Line_in[7])
	Phy_state := CleanStrings(e.Line_in[8])
	Phy_zip := CleanStrings(e.Line_in[9])
	Phy_country := CleanStrings(e.Line_in[10])

	Mailing_street := CleanStrings(e.Line_in[11])
	Mailing_city := CleanStrings(e.Line_in[12])
	Mailing_state := CleanStrings(e.Line_in[13])
	Mailing_zip := CleanStrings(e.Line_in[14])
	Mailing_country := CleanStrings(e.Line_in[15])

	Mcs150_date := CleanStrings(e.Line_in[19])
	Mcs150_mileage_year := CleanStrings(e.Line_in[21])
	Mcs150_mileage := CleanStrings(e.Line_in[20])

	Telephone := CleanStrings(e.Line_in[16])
	Fax := CleanStrings(e.Line_in[17])
	Email_address := CleanStrings(e.Line_in[18])

	out := make([]string, 0)

	out = append(out,
		Dot_number,
		Legal_name,
		Dba_name,
		Carrier_operation,
		Hm_flag,
		Pc_flag,
		Phy_street,
		Phy_city,
		Phy_state,
		Phy_zip,
		Phy_country,
		Mailing_street,
		Mailing_city,
		Mailing_state,
		Mailing_zip,
		Mailing_country,
		Telephone,
		Fax,
		Email_address,
		Mcs150_date,
		Mcs150_mileage,
		Mcs150_mileage_year,
		Dot_add_date,
		Oic_state,
		Nbr_power_units,
		Driver_total)

	//e.Line_in = nil

	//e.Line_in = out

	return out

}

func (e *Fromcsv) DigestOut() Digest {

	//out := make([]string, 0)
	//copy(e.Line_in, out)

	out := e.Line_in

	//fmt.Println(out)
	var dg Digest
	//fmt.Println(len(e.Line_in))
	dg.Key = xxhash.Sum64String(out[0])
	dg.Value = xxhash.Sum64String(strings.Join(out[:], ","))
	dg.Source = out

	//fmt.Println(dg.Source)
	return dg

}

type Digest struct {
	Key    uint64
	Value  uint64
	Source []string
}

type CsvHashDigest struct {
	HashKeyHashValue   map[uint64]uint64
	HashKeySourceValue map[uint64][]string

	//HashKeySourceValue map[uint64][]string
}

func CreateCsvHashDigest() *CsvHashDigest {

	var out CsvHashDigest

	out.HashKeyHashValue = make(map[uint64]uint64)
	out.HashKeySourceValue = make(map[uint64][]string)

	return &out
}

func (e *CsvHashDigest) Append(d Digest) {
	e.HashKeyHashValue[d.Key] = d.Value

	e.HashKeySourceValue[d.Key] = d.Source

	//fmt.Println(d.Key)
	//fmt.Println(e.HashKeySourceValue[d.Key])
	//f.SourceMap[d.Key] = d.Source
}

//will take csv name, write to channel,
//return error or line digests to append
func StreamDigests(path string) (chan []Digest, chan error) {

	maxProcs := runtime.NumCPU()
	digestChannel := make(chan []Digest, BufferSize*maxProcs)
	errorChannel := make(chan error, 1)
	fmt.Println("passed all here")
	base, err := os.Open(path)

	if err != nil {

		fmt.Println("faiild to open path")
	}

	//reader_loc := csv.NewReader(base)

	go func(digestChannel chan []Digest, errorChannel chan error, base io.Reader) {
		wg := &sync.WaitGroup{}
		reader := csv.NewReader(base)

		//reader.Comma = e.config.Separator
		reader.LazyQuotes = true
		reader.FieldsPerRecord = -1
		for {
			lines, eofReached, err := getNextNLines(reader)

			if err != nil {
				wg.Wait()
				close(digestChannel)
				errorChannel <- err
				close(errorChannel)
				return
			}

			wg.Add(1)
			go digestForLines(lines, digestChannel, wg)

			if eofReached {
				break
			}
		}
		wg.Wait()
		close(digestChannel)
		errorChannel <- nil
		close(errorChannel)
	}(digestChannel, errorChannel, base)

	return digestChannel, errorChannel
}

func CreateDigest(line_in []string) Digest {

	// var mc Digest
	// mc.Key = 2222
	// mc.Value = 3333
	// mc.Source = line_in

	if len(line_in) != 26 {
		fmt.Println(len(line_in))
		fmt.Println(line_in)
		var dg Digest

		return dg
	} else {
		var ln_in Fromcsv

		var out_from Fromcsv

		ln_in.Line_in = line_in

		cleanLine := ln_in.CleanSelfLines()

		//fmt.Println(cleanLine)

		out_from.Line_in = cleanLine

		//fmt.Println(out_from.Line_in)

		digest_out := out_from.DigestOut()

		return digest_out
	}

	//return digest_out

}

//type CsvHashKeyHashValueDigest map[uint64]uint64

//type CsvHashKeySourceValueDigest map[uint64][]string

// func CreateCsvHashKeyHashValueDigest () *CsvHashKeyHashValueDigest {
// 	out := make(map[uint64]uint64)

// 	return &CsvHashKeyHashValueDigest{CsvHashKeyHashValueDigest:out}
// }
