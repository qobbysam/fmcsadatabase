package fmcsa

import (
	"fmt"
	"runtime"
)

type Additions struct {
	Key   uint64
	Value []string
}

func (e *Additions) GenFmcsa() (Fmcsa_object, ConstructInfo) {
	outfmcsa, outconstruct := CreateNewFmcsa(e.Value)

	return outfmcsa, outconstruct
}

type Deletions struct {
	Key   uint64
	Value []string
}

type messageType int

type message struct {
	key      uint64
	original []string
	current  []string
	_type    messageType
}

const (
	addition     messageType = iota
	modification messageType = iota
	deletion     messageType = iota
)

type Modifications struct {
	Key      uint64
	Original []string
	Current  []string
}

type Differences struct {
	Additions     []Additions
	Modifications []Modifications
	Deletions     []Deletions
}

// func Diff(superConfig ConfigCsvCsv) (Differences, err) {

// 	base_csvfile := superConfig.CsvfileNameOne

// 	delta_csvfile := superConfig.CsvfileNameTwo

// 	// digest base first

// 	digests_from_base_channel, err = DigestBaseCsv(base_csvfile)

// }

func Diff(superConfig ConfigCsvCsv) (Differences, error) {

	//var lock *sync.Mutex

	fmt.Println("diff started")

	base_csvfile := superConfig.CsvfileNameOne

	delta_csvfile := superConfig.CsvfileNameTwo

	// digest base first

	count := 0

	digests_from_base_channel, errchan := StreamDigests(base_csvfile)

	csv_hash_digest := CreateCsvHashDigest()

	//csv_source_digest := CreateCsvSourceDigest()

	additions := make([]Additions, 0)
	modifications := make([]Modifications, 0)
	deletions := make([]Deletions, 0)

	//lock.Lock()

	//defer lock.Unlock()
	for ms := range digests_from_base_channel {

		for _, d := range ms {
			//fmt.Println(d)
			count = count + 1
			//csv_source_digest.Append(d)
			csv_hash_digest.Append(d)

			// if len(d.Source) != 26 {
			// 	fmt.Println(len(d.Source))

			// }

			//fmt.Println("from Digests sream channel receiving in diff call")
		}
	}
	fmt.Println(len(csv_hash_digest.HashKeySourceValue))
	digests_from_delta_channel, errchan := StreamDigests(delta_csvfile)

	messages_from_stream_differences := streamDifferences(csv_hash_digest, digests_from_delta_channel)

	for msg := range messages_from_stream_differences {
		switch msg._type {
		case addition:
			additions = append(additions, Additions{Key: msg.key, Value: msg.current})
		case modification:
			modifications = append(modifications, Modifications{Key: msg.key, Original: msg.original, Current: msg.current})

		case deletion:
			deletions = append(deletions, Deletions{Key: msg.key, Value: msg.current})

		default:
			continue
		}
	}

	fmt.Println(len(csv_hash_digest.HashKeyHashValue))

	fmt.Println("Total Count Here: ", count)

	fmt.Println("additions total is: ", len(additions))
	fmt.Println("Modifications totalt is: ", len(modifications))
	fmt.Println("Deletions total is:  ", len(deletions))

	if err := <-errchan; err != nil {

		return Differences{}, fmt.Errorf("error in differce process: %v", err)

		fmt.Printf("%v", err)
		fmt.Println("faied in diff")
	}

	return Differences{Additions: additions, Modifications: modifications, Deletions: deletions}, nil

}

func streamDifferences(hashkeyhashvalue *CsvHashDigest, digestChannel chan []Digest) chan message {
	maxProcs := runtime.NumCPU()
	msgChannel := make(chan message, maxProcs*bufferSize)

	go func(base *CsvHashDigest, digestChannel chan []Digest, msgChannel chan message) {
		defer close(msgChannel)

		for digests := range digestChannel {
			for _, d := range digests {

				//fmt.Println(d.Source)
				if baseValue, present := base.HashKeyHashValue[d.Key]; present {
					if baseValue != d.Value {
						// Modification
						msgChannel <- message{_type: modification, key: d.Key, current: d.Source, original: base.HashKeySourceValue[d.Key]}
					}
					// delete from sourceMap so that at the end only deletions are left in base
					delete(base.HashKeySourceValue, d.Key)
				} else {
					// Addition
					msgChannel <- message{_type: addition, current: d.Source, key: d.Key}
				}
			}
		}

		for _, value := range base.HashKeySourceValue {
			msgChannel <- message{_type: deletion, current: value}
		}

	}(hashkeyhashvalue, digestChannel, msgChannel)

	return msgChannel
}
