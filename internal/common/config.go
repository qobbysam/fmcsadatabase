package common

import "github.com/spf13/viper"

type SuperConfig struct {
	DBSource     string `mapstructure:"DB_SOURCE"`
	BaseCsvPath  string `mapstructure:"BASE_CSV_PATH"`
	DeltaCsvPath string `mapstructure:"DELTA_CSV_PATH"`
}

func LoadConfig(path string) (config SuperConfig, err error) {

	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()

	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)

	return
}
